package net.tncy.bookmarket.data;

public class InventoryEntry {
    private Book book;
    private int quantity;
    private float averagePrice;
    private float currentPrice;

    public InventoryEntry() {
    }

    public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) {
        this.book = book;
        this.quantity = quantity;
        this.averagePrice = averagePrice;
        this.currentPrice = currentPrice;
    }

    /** Getters and Setters */

    public Book getBook() {
        return this.book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getAveragePrice() {
        return this.averagePrice;
    }

    public void setAveragePrice(float averagePrice) {
        this.averagePrice = averagePrice;
    }

    public float getCurrentPrice() {
        return this.currentPrice;
    }

    public void setCurrentPrice(float currentPrice) {
        this.currentPrice = currentPrice;
    }

}
